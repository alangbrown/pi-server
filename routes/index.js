/*
 * GET home page.
 */

exports.index = function(req,res){

  res.render('index', { title: "Alan's Raspberry Pi Server" });
}

exports.on = function(req, res){

var gpio = require("pi-gpio");

var pin = req.params.pin;

gpio.open(pin,"output",function(err,value){

        console.log('About to set pin ' + pin + ' to 1');
        gpio.write(pin,1,function(){
                console.log('Setting pin ' + pin + ' to: 1');
                gpio.close(pin);
        });
});


        res.json({  'pin':pin,
                    'status':'ok',
                    'log':'Set pin' + pin + ' to ON'});
        return;
};



exports.off = function(req, res){

var gpio = require("pi-gpio");

var pin = req.params.pin;

gpio.open(pin,"output",function(err,value){

        console.log('About to set pin ' + pin + ' to 0');
        gpio.write(pin,0,function(){
                console.log('Setting pin ' + pin + ' to: 0');
                gpio.close(pin);
        });
});


        res.json({  'pin':pin,
                    'status':'ok',
                    'log':'Set pin' + pin + ' to OFF'});
        return;
};


